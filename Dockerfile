# build server
FROM golang:latest as builder

WORKDIR /app
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o gif-maker

# build webapp
FROM node:latest as node_builder
WORKDIR /app

COPY ./app ./app

RUN npm i -g pnpm
RUN cd app && pnpm install && pnpm build

# both final container
FROM alpine:latest

# create rootless user
RUN adduser -D -g '' toor

WORKDIR /app

# copy files
COPY --from=builder /app/gif-maker /app/gif-maker
COPY --from=builder /app/templates /app/templates
COPY --from=node_builder /app/app-dist /app/app-dist
RUN chown -R toor:toor /app

USER toor

ENV ENVIRONMENT=production

EXPOSE 4000
CMD ["./gif-maker"]
