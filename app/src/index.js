const values = {}
const copyButton = document.getElementById("copyButton")
const errorOutput = document.getElementById("errorOutput")
const successOutput = document.getElementById("successOutput")
const timeout = null

const onChange = (event) => {
    values[event.target.id] = event.target.value

    if (!values.image || !values.redirect) copyButton.disabled = true
    else copyButton.disabled = false
}

const copyLink = () => {
    const { image = "", redirect = "" } = values
    // clear old state
    errorOutput.innerText = ""
    successOutput.innerText = ""

    console.log("aaa")

    if (!image || !redirect) {
        errorOutput.innerText = "Image URL and redirect URL must be set."
        return
    }

    const toCopy = `${window.location.protocol}//${window.location.host}/-?image=${encodeURIComponent(image)}&redirect=${encodeURIComponent(redirect)}`

    // copy and output
    navigator.clipboard.writeText(toCopy)
    successOutput.innerText = "\u{1F44D} Copied text to clipboard!"

    if (timeout) {
        clearTimeout(timeout)
    }
    setTimeout(() => {
        successOutput.innerText = ""
    }, 5000)
}

document.querySelectorAll("input").forEach((el) => {
    el.addEventListener("input", onChange);
})
copyButton.addEventListener("click", copyLink)