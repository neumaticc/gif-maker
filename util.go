package main

import (
	"fmt"
	"io"
	"net/http"
)

func downloadImage(url string) ([]byte, string, error) {
	// Create a new HTTP client with a custom User-Agent header
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, "", err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36")

	response, err := client.Do(req)
	if err != nil {
		return nil, "", err
	}
	defer response.Body.Close()

	// Read data
	imageData, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, "", err
	}

	fmt.Println(len(imageData))

	return imageData, response.Header.Get("Content-Type"), nil
}
