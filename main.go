package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/helmet"
	"github.com/gofiber/template/html/v2"
	"github.com/joho/godotenv"
)

func init() {
	// only load .env.local in DEV
	if os.Getenv("ENVIRONMENT") != "production" {
		err := godotenv.Load(".env.local")
		if err != nil {
			log.Fatal("failed to load env file")
		}
	}
}

func main() {
	// template engine
	engine := html.New("./templates", ".html")
	app := fiber.New(fiber.Config{
		Views: engine,
	})

	// other middleware
	app.Use(helmet.New())

	// serve static web app
	app.Static("/", "./app-dist")

	app.Get("/-", func(c *fiber.Ctx) error {
		image := c.Query("image")
		redirect := c.Query("redirect")

		fmt.Println(image)

		file, format, err := downloadImage(image)
		if err != nil {
			return c.Status(http.StatusInternalServerError).SendString(fmt.Sprintf("failed to download image: %s", err.Error()))
		}

		fmt.Println(format)
		// This is probably bad security-wise but I
		// cba to parse the data with `image` or something.
		if !strings.HasPrefix(format, "image/") {
			return c.Status(http.StatusBadRequest).SendString("bad image format")
		}

		// serve the image with max caching and other headers
		c.Set("Content-Type", format)
		c.Set("Cache-Control", "max-age=31536000")
		c.Set("Refresh", fmt.Sprintf("0;url=%s", redirect))
		return c.Status(http.StatusOK).Send(file)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	log.Fatal(app.Listen(":" + port))
}
