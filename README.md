# gif-maker

A small tool to make funny Discord images where, if clicked, they'll redirect to whatever website you wish.

## try it out

You can try it out on the hosted copy at [gif-maker.neumatic.club](https://gif-maker.neumatic.club):